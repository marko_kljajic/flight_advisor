﻿using AutoMapper;
using FlightAdvisor.DTOs.City;
using FlightAdvisor.DTOs.Comment;
using FlightAdvisor.DTOs.User;
using FlightAdvisor.Models;

namespace FlightAdvisor
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<User, UserRegisterResponseDTO>();
            CreateMap<UserRegisterDTO, User>();
            CreateMap<CityDTO, City>();
            CreateMap<AddCommentDTO, Comment>();
            CreateMap<UpdateCommentDTO, Comment>();
        }
    }
}
