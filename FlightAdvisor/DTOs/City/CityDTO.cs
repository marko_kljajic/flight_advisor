﻿using System.Collections.Generic;
using models = FlightAdvisor.Models;

namespace FlightAdvisor.DTOs.City
{
    public class CityDTO
    {
        public string Name { get; set; }
        public string Country { get; set; }
        public string Description { get; set; }
        public List<models.Comment> Comments { get; set; } //sto ne moze ovo?
    }
}
