﻿namespace FlightAdvisor.DTOs.City
{
    public class NumberOfCommentsDTO
    {
        public int? NumberOfComments { get; set; }
    }
}
