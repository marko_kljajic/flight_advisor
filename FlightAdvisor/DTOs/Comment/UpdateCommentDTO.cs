﻿using System;

namespace FlightAdvisor.DTOs.Comment
{
    public class UpdateCommentDTO
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public DateTime TimeOfUpdate { get; set; } = DateTime.Now;
    }
}
