﻿namespace FlightAdvisor.DTOs.Flight
{
    public class FlightDTO
    {
        public string SourceAirportName { get; set; }
        public string DestinationAirportName { get; set; }
    }
}
