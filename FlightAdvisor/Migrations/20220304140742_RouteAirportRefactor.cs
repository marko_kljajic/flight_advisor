﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace FlightAdvisor.Migrations
{
    public partial class RouteAirportRefactor : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_Routes",
                table: "Routes");

            migrationBuilder.DropColumn(
                name: "Id",
                table: "Routes");

            migrationBuilder.DropColumn(
                name: "DestinationAirportCode",
                table: "Routes");

            migrationBuilder.DropColumn(
                name: "SourceAirportCode",
                table: "Routes");

            migrationBuilder.RenameColumn(
                name: "AirlineID",
                table: "Routes",
                newName: "AirlineId");

            migrationBuilder.AlterColumn<string>(
                name: "Equipment",
                table: "Routes",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "AirlineId",
                table: "Routes",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "AirlineCode",
                table: "Routes",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AddColumn<int>(
                name: "RouteId",
                table: "Routes",
                nullable: false,
                defaultValue: 0)
                .Annotation("SqlServer:Identity", "1, 1");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Routes",
                table: "Routes",
                column: "RouteId");

            migrationBuilder.CreateIndex(
                name: "IX_Routes_DestinationAirportId",
                table: "Routes",
                column: "DestinationAirportId");

            migrationBuilder.CreateIndex(
                name: "IX_Routes_SourceAirportId",
                table: "Routes",
                column: "SourceAirportId");

            migrationBuilder.AddForeignKey(
                name: "FK_Routes_Airports_DestinationAirportId",
                table: "Routes",
                column: "DestinationAirportId",
                principalTable: "Airports",
                principalColumn: "AirportId");

            migrationBuilder.AddForeignKey(
                name: "FK_Routes_Airports_SourceAirportId",
                table: "Routes",
                column: "SourceAirportId",
                principalTable: "Airports",
                principalColumn: "AirportId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Routes_Airports_DestinationAirportId",
                table: "Routes");

            migrationBuilder.DropForeignKey(
                name: "FK_Routes_Airports_SourceAirportId",
                table: "Routes");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Routes",
                table: "Routes");

            migrationBuilder.DropIndex(
                name: "IX_Routes_DestinationAirportId",
                table: "Routes");

            migrationBuilder.DropIndex(
                name: "IX_Routes_SourceAirportId",
                table: "Routes");

            migrationBuilder.DropColumn(
                name: "RouteId",
                table: "Routes");

            migrationBuilder.RenameColumn(
                name: "AirlineId",
                table: "Routes",
                newName: "AirlineID");

            migrationBuilder.AlterColumn<string>(
                name: "Equipment",
                table: "Routes",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AlterColumn<string>(
                name: "AirlineID",
                table: "Routes",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AlterColumn<string>(
                name: "AirlineCode",
                table: "Routes",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AddColumn<int>(
                name: "Id",
                table: "Routes",
                type: "int",
                nullable: false,
                defaultValue: 0)
                .Annotation("SqlServer:Identity", "1, 1");

            migrationBuilder.AddColumn<string>(
                name: "DestinationAirportCode",
                table: "Routes",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SourceAirportCode",
                table: "Routes",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_Routes",
                table: "Routes",
                column: "Id");
        }
    }
}
