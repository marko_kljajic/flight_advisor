﻿using System;

namespace FlightAdvisor.Models
{
    public class Comment
    {
        public int CommentId { get; set; }
        public string Description { get; set; }
        public DateTime TimeOfCreation { get; set; } = DateTime.Now;
        public DateTime TimeOfUpdate { get; set; } = DateTime.Now;
        
        public int CityId { get; set; }
        public City City { get; set; }

        public int UserId { get; set; }
        public User User { get; set; }
    }
}
