﻿using AutoMapper;
using FlightAdvisor.Data;
using FlightAdvisor.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Threading.Tasks;

namespace FlightAdvisor.Services.AirportService
{
    public class AirportService : IAirportService
    {
        public AirportService(DataContext context,IMapper mapper)
        {
            Context = context;
            Mapper = mapper;
        }

        private DataContext Context { get; }
        private IMapper Mapper { get; }

        public async Task<ServiceResponse<string>> Import()
        {
            var counter = 0;
            try 
            {
                var fileName = ConfigurationManager.AppSettings["airports"];

                using (System.IO.StreamReader sourceFile = new System.IO.StreamReader(fileName))
                {
                    
                    string line;
                    while ((line = sourceFile.ReadLine()) != null)
                    {
                        var tokens = line.Split(",");
                        for(int i = 0; i < tokens.Length; i++)
                        {
                            tokens[i] = tokens[i].Trim('\"');
                        }
                        
                        if ( await Context.Cities.AnyAsync(c => c.Name.ToLower() == tokens[2].ToLower()) && !(await Context.Airports.AnyAsync(a => a.AirportId == Int32.Parse(tokens[0]))))
                        {
                            var city = await GetCity(tokens[2]);

                            Context.Airports.Add(new Airport() 
                            {                              
                                AirportId = Int32.Parse(tokens[0]),
                                Name = tokens[1],
                                City = GetCity(tokens[2]).Result,
                                IATA = tokens[4],
                                ICAO = tokens[5],
                                Latitude = Double.Parse(tokens[6]),
                                Longitude = Double.Parse(tokens[7]),
                                Altitude = Int32.Parse(tokens[8]),
                                Timezone = Int32.Parse(tokens[9]),
                                DST = tokens[10],
                                TZ = tokens[11],
                                Type = tokens[12],
                                Source = tokens[13]
                            });

                            await Context.SaveChangesAsync();
                            counter++;
                        }
                    }
                }

                if(counter > 0) 
                {
                    return new ServiceResponse<string>(true, "You have successfuly added airport(s)!");
                }
            }
            catch (Exception ex) 
            {
                return new ServiceResponse<string>(false, ex.Message);
            }

            return new ServiceResponse<string>(false, "You have already added all airports for current cities!");
        }

        public async Task<City> GetCity(string cityName)
        {
            var city = await Context.Cities.FirstOrDefaultAsync(c => c.Name.ToLower() == cityName.ToLower());
            return city;
        }
    }
}
