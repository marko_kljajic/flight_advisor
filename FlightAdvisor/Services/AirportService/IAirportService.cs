﻿using FlightAdvisor.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FlightAdvisor.Services.AirportService
{
    public interface IAirportService
    {
        Task<ServiceResponse<string>> Import();
    }
}
