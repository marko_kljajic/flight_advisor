﻿using AutoMapper;
using FlightAdvisor.Data;
using FlightAdvisor.DTOs.City;
using FlightAdvisor.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FlightAdvisor.Services.CityService
{   
    public class CityService : ICityService
    {
        public CityService(DataContext context,IMapper mapper)
        {
            Context = context;
            Mapper = mapper;
        }

        private DataContext Context { get; }
        private IMapper Mapper { get; }

        public async Task<ServiceResponse<CityDTO>> AddCity(CityDTO cityDTO)
        {
            if (await CityExists(cityDTO.Name))
            {
                return new ServiceResponse<CityDTO>(false, "City already exists");
            }
            try { 
                var city = Mapper.Map<City>(cityDTO);

                await Context.Cities.AddAsync(city);
                await Context.SaveChangesAsync();

                return new ServiceResponse<CityDTO>(true, $"You have added a new city: {cityDTO.Name}!");
            }
            catch (Exception ex)
            {
                return new ServiceResponse<CityDTO>(false, ex.Message );
            }
        }

        public async Task<ServiceResponse<List<City>>> GetAll(NumberOfCommentsDTO numberOfComments)
        {
            ServiceResponse<List<City>> response = new ServiceResponse<List<City>>();

            try { 
                if (numberOfComments.NumberOfComments == null)
                {
                    response.Data = await Context.Cities.Include(c => c.Comments).ToListAsync();
                }
                else if (numberOfComments.NumberOfComments == 0)
                {
                    response.Data = await Context.Cities.ToListAsync();
                }
                else
                {
                    response.Data = await Context.Cities.ToListAsync();
                    var comments = await Context.Comments.Take((int)numberOfComments.NumberOfComments).ToListAsync();
                }
                return new ServiceResponse<List<City>>(response.Data, true, "All cities and comments for each one.");
            }
            catch (Exception ex) 
            {
                return new ServiceResponse<List<City>>(false, ex.Message);
            }
        }

        public async Task<ServiceResponse<City>> Search(SearchCityDTO searchCityDTO)
        {
            ServiceResponse<City> response = new ServiceResponse<City>();
            try 
            { 
                if(!await CityExists(searchCityDTO.Name))
                {
                    return new ServiceResponse<City>(false, "City doesn't exists!");
                }

                if (searchCityDTO.NumberOfComments == null)
                {
                    response.Data = await Context.Cities.SingleOrDefaultAsync(c => c.Name == searchCityDTO.Name);
                    var comments = await Context.Comments.ToListAsync();
                }
                else if (searchCityDTO.NumberOfComments == 0)
                {
                    response.Data = await Context.Cities.SingleOrDefaultAsync(c => c.Name == searchCityDTO.Name);
                }
                else
                {
                    response.Data = await Context.Cities.SingleOrDefaultAsync(c => c.Name == searchCityDTO.Name);
                    var comments = await Context.Comments.Take((int)searchCityDTO.NumberOfComments).ToListAsync();
                }

                return new ServiceResponse<City>(response.Data, true, $"{searchCityDTO.Name} city and {searchCityDTO.NumberOfComments} comments for it!");
            }
            catch (Exception ex) 
            {
                return new ServiceResponse<City>(false, ex.Message);
            }
        }

        private async Task<bool> CityExists(string name)
        {
            if (await Context.Cities.AnyAsync(c => c.Name.ToLower() == name.ToLower())) return true;

            return false;
        }
    }
}
