﻿using FlightAdvisor.DTOs.City;
using FlightAdvisor.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FlightAdvisor.Services.CityService
{
    public interface ICityService
    {
        Task<ServiceResponse<CityDTO>> AddCity(CityDTO cityDTO);
        Task<ServiceResponse<List<City>>> GetAll(NumberOfCommentsDTO numberOfComments);
        Task<ServiceResponse<City>> Search(SearchCityDTO searchCityDTO);
    }
}
