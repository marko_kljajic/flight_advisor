﻿using AutoMapper;
using FlightAdvisor.Data;
using FlightAdvisor.DTOs.Comment;
using FlightAdvisor.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace FlightAdvisor.Services.CommentService
{
    public class CommentService : ICommentService
    {
        public CommentService(DataContext context, IMapper mapper,IHttpContextAccessor httpContextAccessor)
        {
            Context = context;
            Mapper = mapper;
            HttpContextAccessor = httpContextAccessor;
        }

        private DataContext Context { get; }
        private IMapper Mapper { get; }
        public IHttpContextAccessor HttpContextAccessor { get; }

        private int GetUserId() => int.Parse(HttpContextAccessor.HttpContext.User.FindFirstValue(ClaimTypes.NameIdentifier));

        public async Task<ServiceResponse<string>> AddComment(AddCommentDTO addCommentDTO)
        {
            if (await CityExists(addCommentDTO.CityID))
            {
                var comment = Mapper.Map<Comment>(addCommentDTO);

                comment.UserId = GetUserId();

                await Context.Comments.AddAsync(comment);
                await Context.SaveChangesAsync();

                var city = await Context.Cities.FirstOrDefaultAsync(c => c.CityId == comment.CityId);

                return new ServiceResponse<string>(true, $"You have succesfully added comment for city: {city.Name} !");
            }
            return new ServiceResponse<string>(false, "City doesnt exist! ");
        }

        public async Task<ServiceResponse<string>> DeleteComment(DeleteCommentDTO deleteCommentDTO)
        {
            try
            {
                User user = await Context.Users.FirstOrDefaultAsync(u => u.UserId == GetUserId());

                Comment commentForCheck = await Context.Comments
                    .FirstOrDefaultAsync(c => c.CommentId == deleteCommentDTO.Id);

                if (commentForCheck.UserId != user.UserId)
                {
                    return new ServiceResponse<string>(false, "You can't delete this comment beacuse you didn't create it!");
                }

                Context.Comments.Remove(commentForCheck);
                await Context.SaveChangesAsync();

                return new ServiceResponse<string>(true, $"You have succesfully deleted comment with id: {commentForCheck.CommentId}!");
            }
            catch (Exception ex)
            {
                return new ServiceResponse<string>(false, ex.Message);
            }

            
        }

        public async Task<ServiceResponse<string>> UpdateComment(UpdateCommentDTO updateCommentDTO)
        {
            try 
            { 
                Comment commentForUpdate = await Context.Comments.FirstOrDefaultAsync(c=> c.CommentId == updateCommentDTO.Id);

                if (commentForUpdate.UserId != GetUserId())
                {
                    return new ServiceResponse<string>(false, "You can't update this comment beacuse you didn't create it!");
                }

                commentForUpdate.Description = updateCommentDTO.Description;
                commentForUpdate.TimeOfUpdate = updateCommentDTO.TimeOfUpdate;

                Context.Comments.Update(commentForUpdate);
                await Context.SaveChangesAsync();

                return new ServiceResponse<string>(true, $"You have succesfully updated comment with id: {commentForUpdate.CommentId}!");
            }
            catch (Exception ex) 
            {
                return new ServiceResponse<string>(false, ex.Message);
            }
        }

        private async Task<bool> CityExists(int Id)
        {
            if (await Context.Cities.AnyAsync(c => c.CityId == Id)) return true;

            return false;
        }
    }
}
