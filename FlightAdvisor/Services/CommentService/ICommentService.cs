﻿using FlightAdvisor.DTOs.Comment;
using FlightAdvisor.Models;
using System.Threading.Tasks;

namespace FlightAdvisor.Services.CommentService
{
    public interface ICommentService
    {
        Task<ServiceResponse<string>> AddComment(AddCommentDTO addCommentDTO);
        Task<ServiceResponse<string>> UpdateComment(UpdateCommentDTO updateCommentDTO);
        Task<ServiceResponse<string>> DeleteComment(DeleteCommentDTO updateCommentDTO);
    }
}
