﻿using FlightAdvisor.Data;
using FlightAdvisor.DTOs.Flight;
using FlightAdvisor.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FlightAdvisor.Services.FlightService
{
    public class FlightService : IFlightService
    {
        public FlightService(DataContext context)
        {
            Context = context;
        }

        private DataContext Context { get; }

        public async Task<ServiceResponse<Route>> Flight(FlightDTO flightDTO)
        {            
            var sourceAirport = await Context.Airports.FirstOrDefaultAsync(a => a.City.Name == flightDTO.SourceAirportName);
            var destinationAirport = await Context.Airports.FirstOrDefaultAsync(a => a.City.Name == flightDTO.DestinationAirportName);
            try 
            { 
                var routeFromSource = await Context.Routes.Where(r => r.SourceAirportId == sourceAirport.AirportId && r.DestinationAirportId == destinationAirport.AirportId)
                    .OrderBy(r => r.Price)
                    .ToListAsync();

                var cheapestFlight = routeFromSource.First();

                return new ServiceResponse<Route>(cheapestFlight, true, "We have found you a direct route to your destination!");
            }
            catch 
            {
                return new ServiceResponse<Route>(false, "We haven't found you a direct route to your destination!");
            }
        }
    }
}
