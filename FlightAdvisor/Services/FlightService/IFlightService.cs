﻿using FlightAdvisor.DTOs.Flight;
using FlightAdvisor.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FlightAdvisor.Services.FlightService
{
    public interface IFlightService
    {
        Task<ServiceResponse<Route>> Flight(FlightDTO flightDTO);
    }
}
