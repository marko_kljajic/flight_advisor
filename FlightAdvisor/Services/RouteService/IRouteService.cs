﻿using FlightAdvisor.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FlightAdvisor.Services.RouteService
{
    public interface IRouteService 
    {
        Task<ServiceResponse<string>> Import();
    }
}
