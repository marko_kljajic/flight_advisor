﻿using AutoMapper;
using FlightAdvisor.Data;
using FlightAdvisor.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Resources;
using System.Configuration;

namespace FlightAdvisor.Services.RouteService
{
    public class RouteService : IRouteService
    {
        public RouteService(DataContext context, IMapper mapper)
        {
            Context = context;
            Mapper = mapper;
        }

        private DataContext Context { get; }
        private IMapper Mapper { get; }

        public async Task<ServiceResponse<string>> Import()
        {
            var counter = 0;
            try
            {
                var fileName  =  ConfigurationManager.AppSettings["routes"];

                using (System.IO.StreamReader sourceFile = new System.IO.StreamReader(fileName))
                {
                    string line;
                    while ((line = sourceFile.ReadLine()) != null)
                    {
                        var tokens = line.Split(",");

                        if (tokens[3] == "\\N" || tokens[5] == "\\N")
                            continue;

                        if (await Context.Airports.AnyAsync(a => a.AirportId == Int32.Parse(tokens[3])))
                        {
                            if(await Context.Airports.AnyAsync(a => a.AirportId == Int32.Parse(tokens[5])))
                            {
                                Context.Routes.Add(new Route()
                                {

                                    AirlineCode = tokens[0],
                                    AirlineId = tokens[1],
                                    // SourceAirportCode = tokens[2],
                                    SourceAirportId = Int32.Parse(tokens[3]),
                                    // DestinationAirportCode = tokens[4],
                                    DestinationAirportId = Int32.Parse(tokens[5]),
                                    Codeshare = tokens[6],
                                    Stops = Int32.Parse(tokens[7]),
                                    Equipment = tokens[8],
                                    Price = Double.Parse(tokens[9]),

                                });

                                await Context.SaveChangesAsync();
                                counter++;
                            }
                        }
                    }
                }
                if (counter > 0)
                {
                    return new ServiceResponse<string>(false, "You have successfuly added route(s)!");
                }
            }
            catch (Exception ex)
            {
                return new ServiceResponse<string>(false, ex.Message);
            }
            return new ServiceResponse<string>(false, "You have already added all airports for current cities!");
        }
    }
}
