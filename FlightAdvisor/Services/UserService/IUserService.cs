﻿using FlightAdvisor.DTOs.User;
using FlightAdvisor.Models;
using System.Threading.Tasks;

namespace FlightAdvisor.Services.UserService
{
    public interface IUserService
    {
        Task<ServiceResponse<UserRegisterResponseDTO>> Register(UserRegisterDTO userRegisterDTO);
        Task<ServiceResponse<string>> Login(UserLoginDTO userLoginDTO);
    }
}
